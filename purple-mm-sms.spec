%global smscommit a48071a9787167fdbf10673cb6e076018e74f041

Name:           purple-mm-sms
Version:        0.1.7
Release:        1%{?dist}
Summary:        A libpurple plugin for sending and receiving SMS via Modemmanager

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/purple-mm-sms
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/purple-mm-sms/-/archive/%{smscommit}/purple-mm-sms-%{smscommit}.tar.gz

BuildRequires:  gcc
BuildRequires:  pkgconfig(purple)
BuildRequires:  pkgconfig(mm-glib)

%description
A libpurple plugin for sending and receiving SMS via Modemmanager


%prep
%autosetup -p1 -n purple-mm-sms-%{smscommit}

%build
make %{?_smp_mflags}


%install
%make_install

%files
%{_libdir}/purple-2/mm-sms.so
%{_datadir}/pixmaps/*
%doc README.md


%changelog
* Mon Oct 05 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.7-1
- Update to v0.1.7
* Fri Jun 26 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.3-1
- First build for opensuse


